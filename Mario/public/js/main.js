import Camera from './Camera.js';
import {createLevelLoader} from './loaders/level.js'; 
import {loadEntities} from './entities.js';
import Timer from './Timer.js';
import {setupKeyboard} from './input.js';
import {createCollisionLayer} from './layers/collision.js';
import Entity from './Entity.js';
import PlayerController from './traits/PlayerController.js';

function createPlayerEnv(playerEntity) {
	const playerEnv = new Entity();
	const playerControl = new PlayerController();
	playerControl.checkpoint.set(64, 64);
	playerControl.setPlayer(playerEntity);
	playerEnv.addTrait(playerControl);
	return playerEnv;
}

async function main(canvas) {
	const ctx = canvas.getContext('2d');

	const entityFactory = await loadEntities();
	const loadLevel = await createLevelLoader(entityFactory);

	const level = await loadLevel('1-1');

	const camera = new Camera();
		
	const mario = entityFactory.mario();

	//level.comp.layers.push(createCollisionLayer(level));

	const playerEnv = createPlayerEnv(mario);
	level.entities.add(playerEnv);
		 
	const input = setupKeyboard(mario);
	input.listenTo(window);

	const timer = new Timer(1/60);

	timer.Update = function Update(deltaTime) {
			level.Update(deltaTime);

			camera.pos.x = Math.max(0, mario.pos.x - 100);
			
			level.comp.draw(ctx, camera);

	}

	timer.start();
}

const canvas = document.getElementById('screen');
main(canvas);
	
		
/*mario.addTrait({
			NAME: 'hacktrait',
			spawnTimeout: 0,
			obstruct() {

			},
			Update(mario, deltaTime) {
				if (this.spawnTimeout > 0.1 && mario.vel.y < 0) {
					const spawn = createMario();
					spawn.pos.x = mario.pos.x;
					spawn.pos.y = mario.pos.y;
					spawn.vel.y = mario.vel.y - 200;
					level.entities.add(spawn);
					this.spawnTimeout = 0;
				}
				this.spawnTimeout += deltaTime;
			}
		})*/


